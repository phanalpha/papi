import { Wheel } from '@/domain/Wheel';
import type { WheelJSON } from '@/domain/Wheel';
import request from '@/utils/request';
import { noop } from 'lodash';

export type LoginParamsType = {
  _username: string;
  _password: string;
};

export async function login(params: LoginParamsType) {
  const data = await request<WheelJSON>('/api/v1/login', {
    method: 'POST',
    data: params,
    errorHandler: undefined,
  });

  return Wheel.bless(data);
}

export async function logout() {
  await request('/api/v1/logout', { method: 'POST', errorHandler: noop });
}
