import { Wheel } from '@/domain/Wheel';
import type { WheelJSON } from '@/domain/Wheel';
import request from '@/utils/request';

export async function whoami() {
  const data = await request<WheelJSON>('/api/v1/login', { errorHandler: undefined });

  return Wheel.bless(data);
}
