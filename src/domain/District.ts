export class District {
  static bless(data: DistrictJSON): District {
    return new District(data.id, data.name, data.code, data.parent && District.bless(data.parent));
  }

  constructor(
    readonly id: number,
    readonly name: string,
    readonly code: string,
    readonly parent?: District,
  ) {}

  get fullName(): string[] {
    return (this.parent?.fullName || []).concat(this.name);
  }

  contains(code: string) {
    return code.startsWith(this.code.replace(/(00)*$/, ''));
  }
}

export type DistrictJSON = {
  readonly id: number;
  readonly name: string;
  readonly code: string;
  readonly parent?: DistrictJSON;
};
