import moment from 'moment';
import type { Moment } from 'moment';
import { Aptitude } from './Aptitude';
import type { AptitudeJSON } from './Aptitude';

export class Wheel {
  static Anonymous = new Wheel(-1, '', '', false, [], moment());

  static bless(data: WheelJSON) {
    return new Wheel(
      data.id,
      data.username,
      data.realname || '',
      data.senior,
      (data.aptitudes || []).map(Aptitude.bless),
      moment(data.created_at),
      (data.removed_at && moment(data.removed_at)) || undefined,
    );
  }

  constructor(
    readonly id: number,
    readonly username: string,
    readonly realname: string | undefined,
    readonly senior: boolean,
    readonly aptitudes: Aptitude[],
    readonly createdAt: Moment,
    readonly removedAt?: Moment,
  ) {}
}

export type WheelJSON = {
  readonly id: number;
  readonly username: string;
  readonly realname?: string;
  readonly senior: boolean;
  readonly aptitudes?: AptitudeJSON[];
  readonly created_at: string;
  readonly removed_at?: string;
};
