import { District } from './District';
import type { DistrictJSON } from './District';
import { Power } from './Power';
import type { PowerJSON } from './Power';

export class Aptitude {
  static bless(data: AptitudeJSON) {
    return new Aptitude(data.id, data.site && District.bless(data.site), Power.bless(data.power));
  }

  constructor(readonly id: number, readonly site: District | undefined, readonly power: Power) {}
}

export type AptitudeJSON = {
  readonly id: number;
  readonly site?: DistrictJSON;
  readonly power: PowerJSON;
};
