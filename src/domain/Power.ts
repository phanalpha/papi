export enum Intent {
  Universal = '~',
}

export class Power {
  static bless(data: PowerJSON) {
    return new Power(data.id, data.name, data.permanent_id);
  }

  constructor(readonly id: number, readonly name: string, readonly permanentId: Intent) {}
}

export type PowerJSON = {
  readonly id: number;
  readonly name: string;
  readonly permanent_id: Intent;
};
