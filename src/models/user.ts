import type { Effect, Reducer } from 'umi';

import { whoami } from '@/services/user';
import { Wheel } from '@/domain/Wheel';

export type UserModelState = {
  whoami?: Wheel;
};

export type UserModelType = {
  namespace: 'user';
  state: UserModelState;
  effects: {
    whoami: Effect;
  };
  reducers: {
    saveCurrentUser: Reducer<UserModelState>;
  };
};

const UserModel: UserModelType = {
  namespace: 'user',

  state: {
    whoami: Wheel.Anonymous,
  },

  effects: {
    *whoami(_, { call, put }) {
      try {
        const wheel = yield call(whoami);
        yield put({
          type: 'saveCurrentUser',
          payload: wheel,
        });
      } catch {
        yield put({
          type: 'saveCurrentUser',
        });
      }
    },
  },

  reducers: {
    saveCurrentUser(state, action) {
      return {
        ...state,
        whoami: action.payload || Wheel.Anonymous,
      };
    },
  },
};

export default UserModel;
