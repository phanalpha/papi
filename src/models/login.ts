import { stringify } from 'querystring';
import type { Reducer, Effect } from 'umi';
import { history } from 'umi';

import { login, logout } from '@/services/login';
/* import { setAuthority } from '@/utils/authority'; */
import { getPageQuery } from '@/utils/utils';
import { message } from 'antd';

export type StateType = {
  ok?: boolean;
};

export type LoginModelType = {
  namespace: string;
  state: StateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {
    changeLoginStatus: Reducer<StateType>;
  };
};

const Model: LoginModelType = {
  namespace: 'login',

  state: {
    ok: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      try {
        yield call(login, payload);
        yield put({
          type: 'changeLoginStatus',
          payload: true,
        });
        // Login successfully

        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        message.success('🎉 🎉 🎉  登录成功！');
        let { redirect } = params as { redirect: string };
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }
        history.replace(redirect || '/');
      } catch {
        yield put({
          type: 'changeLoginStatus',
          payload: false,
        });
      }
    },

    *logout(_, { call }) {
      yield call(logout);

      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      /* setAuthority(payload.currentAuthority); */
      return {
        ...state,
        ok: payload.ok,
      };
    },
  },
};

export default Model;
